-- Options are automatically loaded before lazy.nvim startup
-- Default options that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/options.lua
-- Add any additional options here
vim.opt.relativenumber = false
-- Autoformatting can break stuff; turn it off.
vim.g.autoformat = false
-- sane (un-)tabbing unless overridden
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true

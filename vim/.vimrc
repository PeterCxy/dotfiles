" Load default vimrc
source $VIMRUNTIME/defaults.vim

" 4 spaces, never use tab by default
set shiftwidth=4 softtabstop=4 expandtab

" In these files, use 2 spaces
autocmd FileType sh setlocal shiftwidth=2 softtabstop=2

set nofixeol

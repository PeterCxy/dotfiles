PeterCxy's Dotfiles
---

These files are supposed to be installed by GNU Stow. For example, if you want to install the dotfiles for `sway` listed in this directory, then clone this repository to your `$HOME`, and then run

```bash
stow sway
```

which will automatially create all the symlinks needed.

All subdirectories (except `fonts`) in this repository are structured with the assumption that you are going to put the repository directly in `$HOME`. If not, you need to change the command for `stow` accordingly.

Note that to install `fonts`, you need `sudo stow -t / fonts` instead.

local wezterm = require("wezterm")
return {
    color_scheme = "Gruvbox dark, medium (base16)",
    font = wezterm.font("FiraCode Nerd Font Mono"),
    font_size = 10.5,
    enable_tab_bar = true,
    use_ime = true,
    scrollback_lines = 65536,
    tab_bar_at_bottom = true,
    window_padding = {
        top = '0.5cell',
        -- We already have the tab bar at bottom
        bottom = '0',
        left = '1cell',
        right = '1cell'
    },
    window_background_opacity = 0.95,
    keys = {
        -- The default Ctrl+Tab conflicts with Sway
        {
            key = 'Tab',
            mods = 'SHIFT',
            action = wezterm.action.ActivateTabRelative(1),
        },
        -- Reload config forcibly
        {
            key = 'r',
            mods = 'CTRL|SHIFT',
            action = wezterm.action.ReloadConfiguration,
        },
        -- Spawn panes in the **DEFAULT** domain
        -- to use the current domain, use Ctrl + Alt + Shift + " or %
        {
            key = '"',
            mods = 'CTRL|SUPER|ALT|SHIFT',
            action = wezterm.action.SplitVertical { domain = 'DefaultDomain' },
        },
        {
            key = '%',
            mods = 'CTRL|SUPER|ALT|SHIFT',
            action = wezterm.action.SplitHorizontal { domain = 'DefaultDomain' },
        },
    },
}
